/*global console*/



var contacts = [];
var filterContacts = [];

function contact (uid, uname, fname, email, pass, age) {
   this.uid = uid, 
   this.username = uname;
   this.firstname = fname;
   this.emailaddress = email;
   this.password = pass;
   this.age = age
    
}




function register() {
    'use strict';
    
    //initialize form variables
    var uname, fname, email, pass, age, errors, errorList, userList, min_filter_age, max_filter_age;
    
	//console = document.console;
	
	
    uname = document.getElementById("username").value;
    fname = document.getElementById("fname").value;
    email = document.getElementById("email").value;
    pass = document.getElementById("pass").value;
    age = parseInt(document.getElementById("age").value);
    min_filter_age = document.getElementById("min_age").value;
    max_filter_age = document.getElementById("max_age").value;
   // var uid = 
    errorList = document.getElementById("error_msgs");
    userList = document.getElementById("user_list");
    
    
   
    
    function checkForDuplicateUsername(given_name){
        var arrIn;
        for (arrIn = 0; arrIn < contacts.length; arrIn++){
            if(given_name === contacts[arrIn].username){
                errors.push("The Username " + contacts[arrIn].username + "Already Exists");
                
            }
        }
        
        
    }
    
    var newid = function(){
       var propId =   Math.floor(Math.random()*11111111111111)
       var arrIn;
       if (contacts.length > 0){
           for (arrIn = 0; arrIn < contacts.length; arrIn++){
            if(propId === contacts[arrIn].uid){
                propId =   Math.floor(Math.random()*1111341111111)
                
            }
        }
           
       }else{
           
       }
        
        return propId
    }
    
    
    errorList.innerHTML = '';
    //initialize errors array
    errors = [];
    
    //validate username
    if (uname.length < 3) {
        errors.push("Please Enter a username of more that three Letters.");
    } else {
        if (uname.search(' ') > 0) {
            errors.push("Your username cannot contain a space.");
        }
        if(contacts.length>0){
            checkForDuplicateUsername(uname);
        }
    }
    
    //validate email, must contain @ and .
    
    if (email.length < 6) {
    	errors.push("Please Enter A valid email");
    } else {
        
        if (email.search('@') < 0 || email.search(".") < 0) {
            errors.push("Please Enter A valid email");
        }
    }
	
	//validate password, must be more than 6 characters
 
	if (email.length < 6) {
    	errors.push("Your Password Should have more than Six characters.");
    }
	
	
	//validate age, must be a numeric character
	
	/*if (IsNumeric(age)) {
		errors.push("Your is Must Be a Number");
	} else if (age < 0) {
		errors.push("Please enter a proper age.");
	}*/
	
    
   var intAge;
    
    intAge = parseInt(age);
    
    if(isNaN(intAge)){
        errors.push("Please Enter a number for your age")
        
    }else{
        
        
    }
	
    
	
	
	//check if the errors array is empty if  empty, register
	//
	
    
   
    
    
	if (errors.length === 0) {
        
        //generate user id for user by getting timestamp
        
        //create new contact Object with form entries
		var contactAdd = new contact(Date.now(), uname, fname, email, pass, age);
        
        
        console.log("contacs name: " + contactAdd.firstname);
		console.log(contactAdd.uid);
        
        //add new contact to the contact array
		contacts.push(contactAdd);
         document.getElementById("calc_button").disabled = false;
          document.getElementById("calc_button").display = "block";
	} else {
	
		console.log(errors);
        console.log(errors.length);
        
        //print out all errors in the error array
        for (var err = 0; err < errors.length; err++) {
            errorList.innerHTML += '<li>'+ errors[err] +'</li>';
            
        }
        
        
	
	}
	
	//console.log("weve done it");
    
    //console.log("CONTACT ARRAY: " + contact);
    //
    userList.innerHTML = '';
     for (var i = 0; i < contacts.length; i++) {
            console.log(contacts[i].uid)
           console.log(contacts[i].firstname);
           console.log(contacts[i].username);
           console.log(contacts[i].emailaddress);
           console.log(contacts[i].age);
           console.log("---------------------");
                    
         
         
         
         userList.innerHTML += '<div id="con_div"><li id="u_list">'+ contacts[i].username +'</li><li id="sub_list">'+contacts[i].firstname+'</li><li id="sub_list">'+contacts[i].emailaddress+'</li><li id="sub_list">'+contacts[i].age+' years old</li></div>';
         
     }
    
	//return false;
	
	
    
}


var clearFilter = function(){
    var  userList = document.getElementById("user_list");
    userList.innerHTML = '';
     for (var i = 0; i < contacts.length; i++) {
         
         
         userList.innerHTML += '<div id="con_div"><li id="u_list">'+ contacts[i].username +'</li><li id="sub_list">'+contacts[i].firstname+'</li><li id="sub_list">'+contacts[i].emailaddress+'</li><li id="sub_list">'+contacts[i].age+' years old</li></div>';
         
     }
//    filterContacts = []
    
}



// calculate average age of users
function calculate_avg(){
    
    var usernum = contacts.length;
    var tot_age, avg, avg_disp ;
    
    tot_age = 0;
    
    avg_disp = document.getElementById("avg_div");
    for (var i = 0; i < contacts.length; i++) {
        
        tot_age += contacts[i].age;
        console.log(contacts[i].age);
    }
    
    avg = tot_age/usernum;
    
    console.log("Total Age is: "+tot_age);
    console.log("Total Number of Users is: "+usernum);
    avg_disp.innerHTML = '<p id="age_dec">The average age is: <br/><span id="age_blow">'+avg+'</span></p>';
    
    alert("The Average age of all contacts is"+ avg);
    
    
}


 function filterAge(){
     var userList = document.getElementById("user_list");
     var min = parseInt(document.getElementById("min_age").value);
    var max= parseInt(document.getElementById("max_age").value);
        console.log(min)
        console.log(max)
        
        
     var filterContacts = contacts.filter(function (contact) {
          return (contact.age >= min && contact.age <= max);
     });
     console.log("Filter COntacts "+filterContacts.length)
     userList.innerHTML = '';
     for (var i = 0; i < filterContacts.length; i++) {
         /*
           console.log(filterContacts[i].firstname);
           console.log(filterContacts[i].username);
           console.log(filterContacts[i].emailaddress);
           console.log(filterContacts[i].age);
           console.log("---------------------");
            */        
         
         userList.innerHTML += '<div id="con_div"><li id="u_list">'+ filterContacts[i].username +'</li><li id="sub_list">'+filterContacts[i].firstname+'</li><li id="sub_list">'+filterContacts[i].emailaddress+'</li><li id="sub_list">'+filterContacts[i].age+' years old</li></div>';
        // console.log("yaaaa")
     }
    
     
        
        
        
}


function sortTrigger(){
    var sortType = document.getElementById("sType").value
    console.log(sortType)
    if (sortType == "name"){
        this.orderByName()
    }else if(sortType == "age"){
       // sortByAge()
        this.sortbyAge()
    }
    
}


function orderByName(){
    
    contacts.sort(function(contacta, contactb){
     var con1=contacta.firstname.toLowerCase(), con2=contactb.firstname.toLowerCase()
     if (con1 < con2) //sort string ascending
      return -1 
     if (con1 > con2)
      return 1
     return 0 //default return value (no sorting)
    })
    var userList = document.getElementById("user_list");
    
    userList.innerHTML = '';
     for (var i = 0; i < contacts.length; i++) {
          /* console.log(contacts[i].firstname);
           console.log(contacts[i].username);
           console.log(contacts[i].emailaddress);
           console.log(contacts[i].age);
           console.log("---------------------");
                    
         */
         
         
         userList.innerHTML += '<div id="con_div"><li id="u_list">'+ contacts[i].username +'</li><li id="sub_list">'+contacts[i].firstname+'</li><li id="sub_list">'+contacts[i].emailaddress+'</li><li id="sub_list">'+contacts[i].age+' years old</li></div>';
         
     }
    
    
}

function sortbyAge(){
      contacts.sort(function(contacta, contactb){
     var con1=contacta.age, con2=contactb.age
     if (con1 < con2) //sort string ascending
      return -1 
     if (con1 > con2)
      return 1
     return 0 //default return value (no sorting)
    })
    var userList = document.getElementById("user_list");
    
    userList.innerHTML = '';
     for (var i = 0; i < contacts.length; i++) {
          /* console.log(contacts[i].firstname);
           console.log(contacts[i].username);
           console.log(contacts[i].emailaddress);
           console.log(contacts[i].age);
           console.log("---------------------");
                    
         */
         
         
         userList.innerHTML += '<div id="con_div"><li id="u_list">'+ contacts[i].username +'</li><li id="sub_list">'+contacts[i].firstname+'</li><li id="sub_list">'+contacts[i].emailaddress+'</li><li id="sub_list">'+contacts[i].age+' years old</li></div>';
         
     }
    
}


//get contacts between an age range
function getContactsForAgeRange (){
    var minAge, maxAge
    
    
}
window.onload = function (){
    console.log("window load")
    if (contacts.length === 0){
         document.getElementById("calc_button").display = "hidden";
        document.getElementById("calc_button").disabled = true;
        
    }else{
        
        console.log("condition false")
    }
    
}
//initialize 
/*function init() {
    'use strict';
    var subform = document.getElementById("reg_form");
    subform.onsubmit = register; 
} */


// used it calls the init function when widow loads
//window.onload = init;

